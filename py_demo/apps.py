from django.apps import AppConfig


class PyDemoConfig(AppConfig):
    name = 'py_demo'
