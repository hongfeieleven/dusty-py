from django.db import models

#定义数据模型

class BnkInf(models.Model):

    bnkNm = models.CharField(max_length=20)
    enabled = models.BooleanField()
    creDt =models.DateTimeField()

    def __str__(self):
        return self.bnkNm + self.enabled

class UsrInf(models.Model):

    usrNm = models.CharField(max_length=64)
    idNo = models.CharField(max_length=18)
    gender = models.CharField(max_length=2)

    def __str__(self):
        return self.usrNm

